import React from "react";
import ReactDOM from "react-dom";
import { ThemeProvider } from "styled-components";
import { AppContextProvider } from "./AppContext";
import "./index.css";
import { theme } from "./theme";
import { Header } from "./components/Header";

import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { Dashboard } from "./dashboard";
import { Signup } from "./signup";
import { Drawer } from "./components/Drawer";

const App = () => {
  return (
    <ThemeProvider theme={theme}>
      <Header />
      <Drawer />
      <Router>
        <Switch>
          <Route exact path="/" component={Signup} />
          <Route path="/dashboard" component={Dashboard} />
        </Switch>
      </Router>
    </ThemeProvider>
  );
};

ReactDOM.render(
  <AppContextProvider>
    <App />
  </AppContextProvider>,
  document.getElementById("root")
);
