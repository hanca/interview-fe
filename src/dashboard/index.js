import React, { useContext } from "react";
import { Container } from "./style";
import { News } from "./News";
import { Weather } from "./Weather";
import { AppContext } from "../AppContext";
import { Graph } from "./Graph";

export const Dashboard = () => {
  const { isDesktop } = useContext(AppContext);

  return (
    <Container isDesktop={isDesktop}>
      {isDesktop && <News />}
      <Weather />
      <Graph />
    </Container>
  );
};
