import React, { useContext, useEffect } from "react";
import {
  NewsContainer,
  NewsTitle,
  NewsInfo,
  StyledInfo,
  StyledIcon,
  NewsRight,
  NewsLeft
} from "./style";
import useFetch from "use-http";
import { NewsApi } from "../../apiKey";
import { AppContext } from "../../AppContext";
import { Source, Time } from "../../assets";
import { getArticle } from "./helpers";

export const News = () => {
  const { loading, data } = useFetch(
    `https://api.nasa.gov/planetary/apod?api_key=${NewsApi}`,
    {},
    []
  );
  let article = {};
  if (data) {
    article = getArticle(data);
  }

  const Info = ({ pic, info }) => (
    <StyledInfo>
      <StyledIcon src={pic} /> {info}
    </StyledInfo>
  );
  return (
    <NewsContainer img={article.image}>
      <NewsTitle> {article.title} </NewsTitle>
      <NewsInfo>
        <NewsLeft>
          <Info pic={Source} info={article.site} />
          <Info pic={Time} info={article.published} />
        </NewsLeft>
        <NewsRight>{article.category}</NewsRight>
      </NewsInfo>
    </NewsContainer>
  );
};
