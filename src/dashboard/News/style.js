import styled from "styled-components";

export const NewsContainer = styled.div`
  ${({
    theme: {
      colors: { weatherBackground },
      spaces: { x4, x5 }
    },
    img
  }) => `
  background-image: url(${img});
  background-size: cover;
  grid-area: news;
  display: flex;
  justify-content: flex-end;
  flex-direction: column;
  padding: ${x4} ${x5};
`}
`;
export const StyledIcon = styled.img`
  ${({
    theme: {
      spaces: { x1 }
    },
    img
  }) => ` margin-right: ${x1};`}
`;

export const StyledInfo = styled.div`
  display: flex;
  align-items: center;
  margin-right: 16px;
`;

export const NewsTitle = styled.div`
  ${({
    theme: {
      spaces: { x3 }
    }
  }) => `
  font-size: 36px;
  color: white;
  font-weight: bold;
  margin-bottom: ${x3}`}
`;

export const NewsInfo = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  color: white;
  font-size: 12px;
`;

export const NewsRight = styled.div`
  ${({
    theme: {
      spaces: { x1 }
    }
  }) => `
  text-transform: uppercase;
  border-bottom: 1px solid white;
  padding-bottom: ${x1};
`}
`;

export const NewsLeft = styled.div`
  display: flex;
  flex-direction: row;
`;
