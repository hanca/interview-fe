import Placeholder from "../../assets/placeholderNews.png";

const getImage = data => (data.media_type === "video" ? Placeholder : data.url);
export const getArticle = data => ({
  title: data.title,
  published: data.date,
  category: "Science",
  site: "SPACE.com",
  type: data.media_type,
  image: getImage(data)
});
