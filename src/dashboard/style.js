import styled from "styled-components";

export const Container = styled.div`
  ${({
    theme: {
      colors: { primaryBackground }
    },
    isDesktop
  }) => `
  background-color:${primaryBackground};
  height: calc(100vh - 64px);
  width: 100vw;
  display: grid;
  grid-template-rows: 1fr 1fr;
  grid-template-columns: ${isDesktop ? `1fr 1fr` : `1fr`};
  grid-template-areas: ${
    isDesktop ? `"news weather" "graphs graphs"` : `"weather" "graphs"`
  };
  `}
`;
