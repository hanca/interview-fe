import styled from "styled-components";
import Tabs from "@material-ui/core/Tabs";

export const Container = styled(Tabs)`
  ${({ isDesktop, isTablet }) => `
  height: ${isDesktop ? `100%` : isTablet ? `30%` : `100%`};
  width:${isDesktop ? `30%` : `100%`};

  & .MuiTabs-flexContainer {
  justify-content: space-around;
  }
  box-shadow: 0 10px 25px rgba(0, 0, 0, 0.3);
  display:flex;
  align-items: center;
  & span {
    visibility: hidden;
  }`}
`;
export const LegendCompany = styled.div`
  ${({
    theme: {
      fontWeight: { bold },
      colors: { textDark }
    }
  }) => `
  font-size: 20px;
  text-transform: uppercase;
  color: ${textDark};
  font-weight: ${bold};
  margin-bottom: 16px;
  white-space: nowrap;
  width: 150px;
  overflow: hidden;
  text-overflow: ellipsis;
`}
`;

export const LegendContainer = styled.div`
  ${({
    theme: {
      spaces: { x2, x4 }
    },
    isDesktop,
    isTablet
  }) => `
  padding: ${isDesktop ? x4 : isTablet ? x2 : 0};
  display:flex;
  flex-direction: ${isDesktop ? "row" : "column"};
  align-items: center;
  cursor:pointer;
  &:hover {
    background-color:#f2f2f2;
  }
  `}
`;

export const LegendInfo = styled.div`
  ${({ isDesktop }) => `
  align-items: ${isDesktop ? "flex-start" : "center"};
  display: flex;
  flex-direction: column;
  width: 70%;
`}
`;
export const LegendValue = styled.div`
  ${({
    theme: {
      fontWeight: { bold },
      colors: { textDark }
    }
  }) => `
  opacity: 0.3;
  color: ${textDark};
  font-size: 16px;
  font-weight: ${bold};
`}
`;

export const Percentage = styled.div`
  ${({
    theme: {
      colors: { positive, negative },
      fontWeight: { bold }
    },
    isNegative,
    isDesktop
  }) =>
    `
  color: ${isNegative ? negative : positive};
  width: 100%;
  display: flex;
  justify-content: ${isDesktop ? "flex-end" : "center"};
  font-size: 30px;
  font-weight: ${bold};
`}
`;
