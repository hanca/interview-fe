import React, { useContext } from "react";
import {
  Container,
  LegendCompany,
  LegendContainer,
  LegendInfo,
  Percentage,
  LegendValue
} from "./style";
import { AppContext } from "../../../AppContext";
import Divider from "@material-ui/core/Divider";

export const Legend = ({ value, handleChange, api }) => {
  const { isDesktop, isTablet, isMobile, company, setCompany } = useContext(
    AppContext
  );
  const tabs = isMobile ? [0, 1] : [0, 1, 2, 3];
  return (
    <Container
      isDesktop={isDesktop}
      isTablet={isTablet}
      orientation={isDesktop ? "vertical" : "horizontal"}
      value={value}
      onChange={handleChange}
      variant="fullWidth"
    >
      {tabs.map((item, i) => (
        <div key={i}>
          <LegendContainer
            isDesktop={isDesktop}
            isTablet={isTablet}
            onClick={() => setCompany(api[item])}
          >
            <LegendInfo isDesktop={isDesktop}>
              <LegendCompany> {api[item].name} </LegendCompany>
              <LegendValue> {api[item].value} </LegendValue>
            </LegendInfo>
            <Percentage
              isDesktop={isDesktop}
              isNegative={api[item].percentage < 0}
            >
              {`${api[item].percentage}%`}
            </Percentage>
          </LegendContainer>
          <Divider variant="middle" />
        </div>
      ))}
    </Container>
  );
};
