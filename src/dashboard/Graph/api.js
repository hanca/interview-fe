import Chance from "chance";

const chance = new Chance();
const api = [
  {
    name: chance.company(),
    value: chance.integer({ min: 0, max: 1000 }),
    percentage: chance.integer({ min: -100, max: 100 }),
    high: chance.integer({ min: 200, max: 500 }),
    low: chance.integer({ min: 0, max: 200 }),
    open: chance.integer({ min: 0, max: 500 }),
    market: chance.integer({ min: 200, max: 500 }),
    divident: `${chance.integer({ min: 0, max: 100 })}%`,
    ratio: chance.integer({ min: 0, max: 100 }),
    monthly: [
      {
        month: "jan",
        value: chance.integer({ min: 0, max: 500 })
      },
      {
        month: "feb",
        value: chance.integer({ min: 0, max: 500 })
      },
      {
        month: "mar",
        value: chance.integer({ min: 0, max: 500 })
      },
      {
        month: "apr",
        value: chance.integer({ min: 0, max: 500 })
      },
      {
        month: "may",
        value: chance.integer({ min: 0, max: 500 })
      },
      {
        month: "jun",
        value: chance.integer({ min: 0, max: 500 })
      },
      {
        month: "jul",
        value: chance.integer({ min: 0, max: 500 })
      },
      {
        month: "aug",
        value: chance.integer({ min: 0, max: 500 })
      },
      {
        month: "sep",
        value: chance.integer({ min: 0, max: 500 })
      },
      {
        month: "oct",
        value: chance.integer({ min: 0, max: 500 })
      },
      {
        month: "nov",
        value: chance.integer({ min: 0, max: 500 })
      },
      {
        month: "dec",
        value: chance.integer({ min: 0, max: 500 })
      }
    ]
  },
  {
    name: chance.company(),
    value: chance.integer({ min: 0, max: 1000 }),
    percentage: chance.integer({ min: -100, max: 100 }),
    high: chance.integer({ min: 200, max: 500 }),
    low: chance.integer({ min: 0, max: 200 }),
    open: chance.integer({ min: 0, max: 500 }),
    market: chance.integer({ min: 200, max: 500 }),
    divident: `${chance.integer({ min: 0, max: 100 })}%`,
    ratio: chance.integer({ min: 0, max: 100 }),
    monthly: [
      {
        month: "jan",
        value: chance.integer({ min: 0, max: 500 })
      },
      {
        month: "feb",
        value: chance.integer({ min: 0, max: 500 })
      },
      {
        month: "mar",
        value: chance.integer({ min: 0, max: 500 })
      },
      {
        month: "apr",
        value: chance.integer({ min: 0, max: 500 })
      },
      {
        month: "may",
        value: chance.integer({ min: 0, max: 500 })
      },
      {
        month: "jun",
        value: chance.integer({ min: 0, max: 500 })
      },
      {
        month: "jul",
        value: chance.integer({ min: 0, max: 500 })
      },
      {
        month: "aug",
        value: chance.integer({ min: 0, max: 500 })
      },
      {
        month: "sep",
        value: chance.integer({ min: 0, max: 500 })
      },
      {
        month: "oct",
        value: chance.integer({ min: 0, max: 500 })
      },
      {
        month: "nov",
        value: chance.integer({ min: 0, max: 500 })
      },
      {
        month: "dec",
        value: chance.integer({ min: 0, max: 500 })
      }
    ]
  },
  {
    name: chance.company(),
    value: chance.integer({ min: 0, max: 1000 }),
    percentage: chance.integer({ min: -100, max: 100 }),
    high: chance.integer({ min: 200, max: 500 }),
    low: chance.integer({ min: 0, max: 200 }),
    open: chance.integer({ min: 0, max: 500 }),
    market: chance.integer({ min: 200, max: 500 }),
    divident: `${chance.integer({ min: 0, max: 100 })}%`,
    ratio: chance.integer({ min: 0, max: 100 }),
    monthly: [
      {
        month: "jan",
        value: chance.integer({ min: 0, max: 500 })
      },
      {
        month: "feb",
        value: chance.integer({ min: 0, max: 500 })
      },
      {
        month: "mar",
        value: chance.integer({ min: 0, max: 500 })
      },
      {
        month: "apr",
        value: chance.integer({ min: 0, max: 500 })
      },
      {
        month: "may",
        value: chance.integer({ min: 0, max: 500 })
      },
      {
        month: "jun",
        value: chance.integer({ min: 0, max: 500 })
      },
      {
        month: "jul",
        value: chance.integer({ min: 0, max: 500 })
      },
      {
        month: "aug",
        value: chance.integer({ min: 0, max: 500 })
      },
      {
        month: "sep",
        value: chance.integer({ min: 0, max: 500 })
      },
      {
        month: "oct",
        value: chance.integer({ min: 0, max: 500 })
      },
      {
        month: "nov",
        value: chance.integer({ min: 0, max: 500 })
      },
      {
        month: "dec",
        value: chance.integer({ min: 0, max: 500 })
      }
    ]
  },
  {
    name: chance.company(),
    value: chance.integer({ min: 0, max: 1000 }),
    percentage: chance.integer({ min: -100, max: 100 }),
    high: chance.integer({ min: 200, max: 500 }),
    low: chance.integer({ min: 0, max: 200 }),
    open: chance.integer({ min: 0, max: 500 }),
    market: chance.integer({ min: 200, max: 500 }),
    divident: `${chance.integer({ min: 0, max: 100 })}%`,
    ratio: chance.integer({ min: 0, max: 100 }),
    monthly: [
      {
        month: "jan",
        value: chance.integer({ min: 0, max: 500 })
      },
      {
        month: "feb",
        value: chance.integer({ min: 0, max: 500 })
      },
      {
        month: "mar",
        value: chance.integer({ min: 0, max: 500 })
      },
      {
        month: "apr",
        value: chance.integer({ min: 0, max: 500 })
      },
      {
        month: "may",
        value: chance.integer({ min: 0, max: 500 })
      },
      {
        month: "jun",
        value: chance.integer({ min: 0, max: 500 })
      },
      {
        month: "jul",
        value: chance.integer({ min: 0, max: 500 })
      },
      {
        month: "aug",
        value: chance.integer({ min: 0, max: 500 })
      },
      {
        month: "sep",
        value: chance.integer({ min: 0, max: 500 })
      },
      {
        month: "oct",
        value: chance.integer({ min: 0, max: 500 })
      },
      {
        month: "nov",
        value: chance.integer({ min: 0, max: 500 })
      },
      {
        month: "dec",
        value: chance.integer({ min: 0, max: 500 })
      }
    ]
  }
];

export default api;
