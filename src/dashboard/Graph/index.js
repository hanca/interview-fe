import React, { useContext, useState, useEffect } from "react";
import { Container } from "./style";
import { AppContext } from "../../AppContext";
import { Legend } from "./Legend";
import api from "./api";
import Visualisation from "./Visualisation";
export const Graph = () => {
  const {
    isDesktop,
    isMobile,
    company,
    setCompany,
    isTablet,
    tabCompany,
    setTabCompany
  } = useContext(AppContext);

  useEffect(() => setCompany(api[0]), []);
  const handleChange = (event, newValue) => {
    setTabCompany(newValue);
  };

  return (
    <Container isDesktop={isDesktop}>
      <Legend api={api} value={tabCompany} handleChange={handleChange} />
      {!isMobile && <Visualisation />}
    </Container>
  );
};
