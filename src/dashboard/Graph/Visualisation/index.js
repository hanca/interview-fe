import React, { useContext } from "react";
import {
  AreaChart,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  ResponsiveContainer,
  Area,
  Label
} from "recharts";
import { AppContext } from "../../../AppContext";
import { Container, Dot } from "./style";

const Visualisation = () => {
  const { company, isDesktop } = useContext(AppContext);
  const monthly = company.monthly;
  const data =
    monthly &&
    monthly.map(({ month, value }) => ({
      name: month,
      capital: value
    }));

  return (
    <ResponsiveContainer
      width={isDesktop ? "80%" : "99%"}
      height={isDesktop ? 500 : 400}
      style={{ bottom: 0, position: "relative" }}
    >
      <AreaChart
        data={data}
        margin={{
          top: 100,
          right: 0,
          left: 0,
          bottom: 0
        }}
        style={{
          bottom: 0,
          position: "relative"
        }}
      >
        <defs>
          <linearGradient id="colorUv" x1="0" y1="0" x2="0" y2="1">
            <stop
              offset="5%"
              stopColor="#rgba(0, 0, 0, 0.2)"
              stopOpacity={0.6}
            />
            <stop offset="95%" stopColor="transparent" stopOpacity={0} />
          </linearGradient>
        </defs>
        <XAxis
          dataKey="name"
          axisLine={false}
          tickLine={false}
          mirror
          padding={{ right: 50 }}
          style={{
            opacity: "0.5",
            color: "black",
            fontSize: "12px",
            fontWeight: 700,
            transform: "translate(0, -20px)"
          }}
        />
        <YAxis
          axisLine={false}
          tickLine={false}
          mirror
          orientation="right"
          style={{
            opacity: "0.5",
            color: "black",
            fontSize: "12px",
            fontWeight: 700,
            transform: "translate(-10px, 0px)"
          }}
        />
        <Tooltip cursor={false} />
        <Area
          activeDot={{
            fill: " #f96332",
            stroke: "rgba(249, 101, 52, 0.3)",
            strokeWidth: 20,
            r: 10
          }}
          type="monotone"
          dataKey="capital"
          stroke="rgba(0, 0, 0, 0.2)"
          fill="url(#colorUv)"
          fillOpacity={0.4}
          animationEasing="ease"
          style={{ cursor: "pointer" }}
        />
      </AreaChart>
    </ResponsiveContainer>
  );
};
export default Visualisation;
