import styled from "styled-components";

export const Container = styled.div`
  ${({ isDesktop }) => `
  grid-area: graphs;
  display: flex;
  flex-direction: ${isDesktop ? `row` : `column`};`}
`;
