const convertToH = date => new Date(date).toTimeString().slice(0, 5);

export const getTemp = (data, initialH, lastH) => {
  const arr = [];
  for (let i = initialH; i <= lastH; i++) {
    arr.push({
      h: convertToH(data.list[i].dt_txt),
      temp: `${Math.floor(data.list[i].main.temp)}º`
    });
  }
  return arr;
};

const getDayFormat = (data, num, isMobile) => ({
  location: data.city.name,
  description: data.list[num].weather[0].main,
  icon: `http://openweathermap.org/img/w/${data.list[num].weather[0].icon}.png`,
  avgTemp: `${Math.floor(data.list[num].main.temp)}º`,
  tempH: getTemp(data, num, isMobile ? num + 5 : num + 7)
});

export const getToday = (data, isMobile) => getDayFormat(data, 0, isMobile);
export const getTomorrow = data => getDayFormat(data, 8);
export const getWeek = data => getDayFormat(data, 16);
