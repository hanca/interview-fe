import React, { useContext, useEffect } from "react";
import {
  Container,
  Section,
  TabsWrap,
  TabButton,
  LocationSection,
  TabsSection,
  Subtitle,
  Title,
  AverageTemp,
  WeatherIcon,
  TempHour,
  Hour
} from "./style";
import useFetch from "use-http";
import { WeatherApi } from "../../apiKey";
import { getTemp, getToday, getWeek, getTomorrow } from "./helpers";
import { AppContext } from "../../AppContext";

export const Weather = () => {
  const { loading, data } = useFetch(
    `https://api.openweathermap.org/data/2.5/forecast?q=Iasi,RO&units=metric&appid=${WeatherApi}`,
    {},
    []
  );
  const { tab, setTab, day, setDay, isMobile } = useContext(AppContext);

  let today, tomorrow, week;
  useEffect(() => {
    if (data) {
      const today = getToday(data, isMobile);
      setDay(today);
    }
  }, [data]);

  if (data) {
    today = getToday(data, isMobile);
    tomorrow = getTomorrow(data);
    week = getWeek(data);
  }
  const handleTabs = (event, newValue) => setTab(newValue);
  return (
    <Container isMobile={isMobile}>
      <Section>
        <LocationSection>
          <WeatherIcon src={day.icon} />
          <Title isMobile={isMobile}> {day.description} </Title>
          <Subtitle isMobile={isMobile}> {day.location} </Subtitle>
        </LocationSection>
        <TabsSection>
          {!isMobile && (
            <TabsWrap value={tab} onChange={handleTabs}>
              <TabButton label="Today" onClick={() => setDay(today)} />
              <TabButton label="Tomorrow" onClick={() => setDay(tomorrow)} />
              <TabButton label="Week" onClick={() => setDay(week)} />
            </TabsWrap>
          )}
          <AverageTemp>{day.avgTemp}</AverageTemp>
        </TabsSection>
      </Section>
      <Section>
        {Object.keys(day).length > 0 &&
          day.tempH.map(({ temp, h }, i) => (
            <TempHour key={i}>
              <Title isMobile={isMobile}>{temp}</Title>
              <Hour isMobile={isMobile}> {h} </Hour>
            </TempHour>
          ))}
      </Section>
    </Container>
  );
};
