import styled from "styled-components";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";

export const Container = styled.div`
  ${({
    theme: {
      colors: { weatherBackground },
      spaces: { x4, x6, x2, x3 }
    },
    isMobile
  }) => `
  grid-area: weather;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  background-color:${weatherBackground};
  padding: ${isMobile ? `${x2} ${x3}` : `${x4} ${x6}`};
  color: white;
  `}
`;

export const Section = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`;

export const TabsSection = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;
`;

export const LocationSection = styled(TabsSection)`
  align-items: flex-start;
`;

export const TabsWrap = styled(Tabs)`
  & .MuiTabs-indicator {
    background-color: white;
  }
`;

export const TabButton = styled(Tab)`
  min-width: 0 !important;
  & span {
    font-size: 11px;
    font-weight: 700;
  }
`;

export const AverageTemp = styled.div`
  color: white;
  font-weight: bold;
  font-size: 100px;
`;

export const Title = styled.div`
  ${({
    theme: {
      fontWeight: { bold },
      colors: { textLight }
    },
    isMobile
  }) => `
  color: ${textLight};
  font-size: ${isMobile ? `16px` : `26px`};
  font-weight: ${bold};
`}
`;

export const Subtitle = styled(Title)`
  ${({
    theme: {
      fontWeight: { bold },
      colors: { textLight }
    },
    isMobile
  }) => `
  opacity: 0.5;
  font-size: ${isMobile ? `12px` : `20px`};
`}
`;

export const WeatherIcon = styled.img`
  height: 90px;
  width: 110px;
`;

export const TempHour = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;
`;

export const Hour = styled(Subtitle)`
  ${({ isMobile }) => `
  font-size: ${isMobile ? `10px` : `16px`};
`}
`;
