export const theme = {
  colors: {
    header: "#f96332",
    weatherBackground: "#ffb236",
    primaryBackground: "#fff",
    textLight: "#fff",
    textGrey: "#b2b2b2",
    textDark: "#000",
    negative: "#ff3636",
    positive: "#63da38"
  },
  spaces: {
    x0: "0",
    x1: "8px",
    x2: "16px",
    x3: "24px",
    x4: "32px",
    x5: "40px",
    x6: "48px",
    x7: "56px",
    x8: "64px",
    x9: "72px",
    x10: "80px",
    x11: "88px",
    x12: "96px"
  },
  fontWeight: {
    regular: 400,
    bold: 700
  }
};

export const mediaDevice = {
  mobile: `(min-width: 320px)`,
  tablet: `(min-width: 768px)`,
  desktop: `(min-width: 1440px)`
};
