import React, { createContext, useState, useEffect } from "react";

const AppContext = createContext();

const AppContextProvider = ({ children }) => {
  const getSize = () => ({
    isDesktop: window.innerWidth >= 1440,
    isTablet: window.innerWidth >= 768,
    isMobile: window.innerWidth < 768
  });
  const [logIn, setLogIn] = useState(false);
  const [device, setDevice] = useState(getSize());
  const [openedDrawer, setOpenedDrawer] = useState(false);
  const [userInfo, setUserInfo] = useState({
    name: "Thomas",
    job: "Photographer",
    email: "thomas@invisionapp.com",
    password: "password",
    notifications: [
      { day: "Monday", enabled: true },
      { day: "Tuesday", enabled: false },
      { day: "Wednesday", enabled: true },
      { day: "Thrusday", enabled: false },
      { day: "Friday", enabled: true }
    ]
  });
  //Weather
  const [tab, setTab] = React.useState(0);
  const [day, setDay] = useState({});
  //Graph
  const [tabCompany, setTabCompany] = useState(0);
  const [company, setCompany] = useState({});
  useEffect(() => {
    const handleResize = () => setDevice(getSize());
    window.addEventListener("resize", handleResize);
    return () => window.removeEventListener("resize", handleResize);
  }, []);

  const defaultContext = {
    ...device,
    openedDrawer,
    setOpenedDrawer,
    userInfo,
    setUserInfo,
    logIn,
    setLogIn,
    setTab,
    tab,
    day,
    setDay,
    tabCompany,
    setTabCompany,
    company,
    setCompany
  };

  return (
    <AppContext.Provider value={defaultContext}>{children}</AppContext.Provider>
  );
};

export { AppContext, AppContextProvider };
