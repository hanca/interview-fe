import styled from "styled-components";

export const InputContainer = styled.div`
  ${({
    theme: {
      colors: { positive, negative },
      spaces: { x2, x1 },
      fontWeight: { bold }
    },
    isCorrect
  }) => `
  border: 1px solid ${isCorrect ? positive : negative};
width: 325px;
height: 50px;
margin: ${x1};
display: flex;
flex-direction: row;
background-color: rgba(255,255,255,0.1);
border-radius: 30px;
position: relative;
align-items: center;
`}
`;

export const InputText = styled.input`
  ${({
    theme: {
      colors: { primaryBackground },
      spaces: { x8, x1, x2 },
      fontWeight: { bold }
    },
    isPassword
  }) => `
background-color: transparent;
border: none;
color: white;
font-size: 13px;
font-weight: ${bold};
position: absolute;
left: ${x8};


&::placeholder{
  font-size: 13px;
  font-weight: ${bold};
  color: white;
${isPassword && `-webkit-text-security: disc;`}
}
`}
`;

export const InputIcon = styled.img`
  width: 100%;
`;

export const IconContainer = styled.div`
  ${({
    theme: {
      spaces: { x3 }
    }
  }) => `
width: 24px;
left: ${x3};
position: absolute;
`}
`;
