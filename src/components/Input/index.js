import React from "react";
import { InputText, InputIcon, InputContainer, IconContainer } from "./style";

export const Input = ({ icon, text, type, handleChange, value, isCorrect }) => (
  <InputContainer isCorrect={isCorrect}>
    <IconContainer>
      <InputIcon src={icon} />
    </IconContainer>
    <InputText
      placeholder={text}
      type={type}
      isPassword={type === "password"}
      onChange={handleChange}
      value={value}
    />
  </InputContainer>
);
