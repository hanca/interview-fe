import React, { useContext } from "react";
import { AppContext } from "../../AppContext";
import {
  Container,
  UserForm,
  SwitchContainer,
  NotificationContainer,
  NotificationTitle
} from "./style";

import { Username, Password, Job, Email } from "../../assets";
import { DrawerTop } from "./DrawerTop";
import { DrawerBottom } from "./DrawerBottom";
import { Input } from "../Input";
import Switch from "@material-ui/core/Switch";
export const Drawer = () => {
  const { openedDrawer, isDesktop, userInfo } = useContext(AppContext);

  return (
    openedDrawer &&
    isDesktop && (
      <Container openedDrawer>
        <DrawerTop />
        <UserForm>
          <Input icon={Username} text={userInfo.name} name="name" />
          <Input icon={Job} text={userInfo.job} />
          <Input icon={Email} text={userInfo.email} />
          <Input icon={Password} text={userInfo.password} type="password" />
        </UserForm>
        <NotificationContainer>
          <NotificationTitle>Email notification</NotificationTitle>
          {userInfo.notifications.map(({ day, enabled }) => (
            <SwitchContainer
              control={<Switch color="default" value={enabled} />}
              label={day}
              labelPlacement="start"
            />
          ))}
        </NotificationContainer>
        <DrawerBottom />
      </Container>
    )
  );
};
