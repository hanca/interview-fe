import styled from "styled-components";

export const Container = styled.div`
  ${({
    theme: {
      spaces: { x8 }
    }
  }) => `  
  display: flex;
  flex-direction: row;
  margin-top: ${x8};`}
`;

export const Circle = styled.div`
  ${({
    theme: {
      spaces: { x1 }
    }
  }) => `
  margin: 0 -${x1};
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: rgba(255,255,255,0.1);
  height: 120px;
  width: 120px;
  border-radius: 50%;
`}
`;

export const UserIcon = styled.img`
  ${({
    theme: {
      spaces: { x1 }
    }
  }) => `
  margin: 0 -${x1};
  height: 120px;
  width: 120px;
  border-radius: 50%;
`}
`;
