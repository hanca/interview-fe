import React from "react";
import { Container, Circle, UserIcon } from "./style";
import { Icon } from "../style";
import { Upload, AvatarBig, Sync } from "../../../assets";

export const DrawerTop = () => (
  <Container>
    <Circle>
      <Icon src={Upload} alt="icon" />
    </Circle>
    <UserIcon src={AvatarBig} />
    <Circle>
      <Icon src={Sync} alt="icon" />
    </Circle>
  </Container>
);
