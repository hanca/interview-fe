import styled from "styled-components";
import FormControlLabel from "@material-ui/core/FormControlLabel";

export const Container = styled.div`
  ${({
    theme: {
      colors: { header }
    },
    openedDrawer
  }) => `
left: 0;
height: calc(100vh - 64px);
width: ${openedDrawer ? `30vw` : 0};
background-color: ${header};
box-shadow: -20px 0 30px rgba(0, 0, 0, 0.3);
display: flex;
align-items: center;
flex-direction: column;
z-index: 100;
position: absolute;
`}
`;

export const Icon = styled.img`
  height: 24px;
  width: 24px;
  cursor: pointer;
`;

export const UserForm = styled.div`
  display: flex;
  flex-direction: column;
  margin-top: 30px;
`;

//Notifications

export const SwitchContainer = styled(FormControlLabel)`
  display: flex;
  justify-content: space-between;
  & .MuiTypography-root {
    font-size: 9px;
    font-weight: 700;
    font-family: "Montserrat";
  }
`;

export const NotificationContainer = styled.div`
  ${({
    theme: {
      colors: { textLight },
      spaces: { x3 }
    }
  }) => `
    display: flex;
    justify-content: center;
    flex-direction: column;
    color: ${textLight};
    width: 325px;
    margin:  ${x3} 0;`};
`;
export const NotificationTitle = styled.div`
  ${({
    theme: {
      spaces: { x2 },
      fontWeight: { bold }
    }
  }) => `
   display: flex;
   justify-content: flex-start;
   margin: ${x2} 0;
   font-size: 14px;
   font-weight: ${bold};
   text-transform: uppercase;`}
`;
