import React, { useContext } from "react";
import { Container, Text } from "./style";
import { Icon } from "../style";
import { Collapse, Logout } from "../../../assets";
import { AppContext } from "../../../AppContext";

export const DrawerBottom = () => {
  const { setOpenedDrawer } = useContext(AppContext);
  return (
    <Container>
      <Icon src={Collapse} onClick={() => setOpenedDrawer(false)} />
      <Text>Saving...</Text>
      <a href="/">
        <Icon src={Logout} />
      </a>
    </Container>
  );
};
