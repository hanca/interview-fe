import styled from "styled-components";

export const Container = styled.div`
  ${({
    theme: {
      spaces: { x8 }
    }
  }) => ` 
  display: flex;
  flex-direction: row;
  margin-bottom: ${x8};
  justify-content: space-around;
  width: 30vw;
  bottom: 0;
  position: fixed;`}
`;

export const Text = styled.div`
  ${({
    theme: {
      colors: { textLight },
      fontWeight: { bold }
    },
    isPassword
  }) => `
  font-size: 14px;
  font-weight: ${bold};
  text-transform: uppercase;
  color : ${textLight};
  opacity: .2;
`}
`;
