import styled from "styled-components";

export const Container = styled.div`
  ${({
    theme: {
      colors: { header },
      spaces: { x4, x8 }
    }
  }) => `
  background-color: ${header};
  height: ${x8}; 
  top:0;
  display:flex;
  justify-content:space-between;
  align-items: center;
  padding: 0px calc(${x4} - 3px) 0px ${x4};
`}
`;

export const Section = styled.div`
  ${({
    theme: {
      spaces: { x4 }
    },
    isLeft,
    isMobile
  }) =>
    `
  align-items: center;
  display: flex;
  justify-content: space-between;
  ${!isMobile &&
    `
    display: grid;
    grid-template-columns:  repeat(
     ${isLeft ? 2 : 3},
     min-content
    );
  grid-gap: ${isLeft ? `103px` : x4};`}
`}
`;

export const UserPicture = styled.img`
  border-radius: 50%;
  cursor: pointer;
`;

export const Text = styled.div`
  ${({
    theme: {
      fontWeight: { bold },
      colors: { textLight }
    }
  }) =>
    `
  font-size: 10px;
  text-transform: uppercase;
  font-weight: ${bold};
  color: ${textLight};
  letter-spacing: 1px;
  cursor:pointer;
`}
`;

export const IconStyle = styled.img`
  cursor: pointer;
`;

export const LogoStyle = styled.div`
  position: absolute;
  left: 50%;
  transform: translate(-50%, 0);
`;
