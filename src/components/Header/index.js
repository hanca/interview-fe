import React, { useContext } from "react";
import { AppContext } from "../../AppContext";
import {
  Container,
  Section,
  UserPicture,
  Text,
  IconStyle,
  LogoStyle
} from "./style";
import {
  Search,
  Hamburger,
  Bookmark,
  Add,
  Logo,
  UserPlaceholder
} from "../../assets";

export const Icon = ({ src, handleClick }) => (
  <IconStyle src={src} alt="logo" onClick={handleClick} />
);

export const Header = () => {
  const { isDesktop, isMobile, setOpenedDrawer } = useContext(AppContext);
  return (
    <Container>
      <Section isLeft isMobile={isMobile}>
        <Icon src={Hamburger} handleClick={() => setOpenedDrawer(true)} />
        {isDesktop && <Text> Widgets </Text>}
      </Section>
      <LogoStyle>
        <Logo />
      </LogoStyle>
      <Section isMobile={isMobile}>
        {!isDesktop && <Icon src={Add} />}
        {!isMobile && (
          <>
            <Icon src={Search}></Icon>
            <UserPicture src={UserPlaceholder} alt="userpic" />
          </>
        )}
        {isDesktop && <Icon src={Bookmark} />}
      </Section>
    </Container>
  );
};
