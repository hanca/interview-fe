import React, { useContext } from "react";
import { AppContext } from "../AppContext";
import Stepper from "@material-ui/core/Stepper";
import Step from "@material-ui/core/Step";
import { StepContainer, Label } from "./style";

export const Steps = () => {
  const { isDesktop, isMobile } = useContext(AppContext);
  const steps = ["Step 1", "Step 2", "Step 3"];

  return (
    <StepContainer desktop={isDesktop}>
      <Stepper activeStep={0} alternativeLabel>
        {steps.map(label => (
          <Step key={label}>
            <Label>{!isMobile && label}</Label>
          </Step>
        ))}
      </Stepper>
    </StepContainer>
  );
};
