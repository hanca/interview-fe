import React from "react";
import { Container, ContentContainer, Mask } from "./style";

export const Wrapper = ({ children }) => (
  <Container>
    <Mask>
      <ContentContainer>{children} </ContentContainer>
    </Mask>
  </Container>
);
