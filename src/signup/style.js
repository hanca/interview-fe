import styled from "styled-components";
import StepLabel from "@material-ui/core/StepLabel";

export const Container = styled.div`
  background-image: linear-gradient(10deg, #000000 0%, #eb5322 100%);
  height: calc(100vh - 64px);
  width: 100vw;
`;

export const Mask = styled.div`
  height: 100%;
  background-image: linear-gradient(
    0deg,
    rgba(255, 255, 255, 0.25) 50%,
    transparent 200%
  );
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
`;

export const ContentContainer = styled.div`
  width: 60vw;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
`;

export const Title = styled.div`
  ${({
    theme: {
      colors: { textLight },
      fontWeight: { bold },
      spaces: { x7 }
    },
    mobile
  }) => `
  font-weight: ${bold};
  color: ${textLight};
  font-size: ${mobile ? `19px` : `40px`};
  text-transform: uppercase;
  letter-spacing: 2.4px;
  margin: ${x7} 0 ;
  `}
`;

export const BottomText = styled.div`
  ${({
    theme: {
      fontWeight: { regular },
      colors: { textLight }
    }
  }) => `
  font-weight: ${regular};
  color: ${textLight};
  opacity: .5;
  font-size: 11px;
  cursor: pointer;
  `}
`;

export const Button = styled.a`
  ${({
    theme: {
      spaces: { x4 },
      fontWeight: { regular }
    }
  }) => `
  width: 325px;
  height: 50px;
  display: flex;
  border-radius: 30px;
  align-items: center;
  justify-content: center;
  background-color: transparent;
  color: white;
  border: 1px solid rgba(255, 255, 255, 0.3);
  margin: ${x4} 0 ;
  font-weight: ${regular};
  font-size: 13px;
  cursor: pointer;
  text-decoration: none;
`}
`;

export const StepContainer = styled.div`
  ${({ desktop }) => `
  width: ${desktop ? `60vw` : `90vw`};
  & .MuiPaper-root {
    background-color: transparent;
    padding: 0;
  }
  `}
`;

export const Label = styled(StepLabel)`
  &:not(:first-child) {
    margin-top: 10px;
  }
  & svg {
    height: 5px;
    width: 5px;
    border-radius: 5px;
    color: white;
    & text {
      display: none;
    }
  }
  & span .MuiStepIcon-active {
    color: transparent;
    height: 20px;
    width: 20px;
    border-radius: 50%;
    border: 1px solid white;
  }
  & .MuiStepLabel-labelContainer span {
    color: white;
    font-size: 10px;
    text-transform: uppercase;
    opacity: 0.4;
    font-weight: 700;
  }
`;
