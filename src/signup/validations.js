export const validations = login => ({
  name: login.name.length > 3 && login.name.toUpperCase(),
  email: /^[0-9?A-z0-9?]+(\.)?[0-9?A-z0-9?]+@[A-z]+\.[A-z]{3}.?[A-z]{0,3}$/g.test(
    login.email
  ),
  password: login.password.length > 7
});
