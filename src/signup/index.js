import React, { useContext, useState } from "react";
import { AppContext } from "../AppContext";
import { Title, BottomText, Button } from "./style";
import { Input } from "../components/Input";
import { Job, Password, Email } from "../assets";
import { Steps } from "./Steps";
import { Wrapper } from "./Wrapper";
import { validations } from "./validations";

export const Signup = () => {
  const { isMobile, userInfo, setUserInfo } = useContext(AppContext);
  const [login, setLogin] = useState({
    name: "",
    email: "",
    password: ""
  });

  const goToDashboard = () => {
    if (
      JSON.stringify(login) ===
      JSON.stringify({
        name: userInfo.name,
        email: userInfo.email,
        password: userInfo.password
      })
    )
      return "/dashboard";
    return "/";
  };

  return (
    <Wrapper>
      <Steps />
      <Title mobile={isMobile}>Create account</Title>
      <>
        <Input
          icon={Job}
          text="Name"
          isCorrect={validations(login).name}
          handleChange={e => setLogin({ ...login, name: e.target.value })}
        />
        <Input
          icon={Email}
          text="Email"
          isCorrect={validations(login).email}
          handleChange={e => setLogin({ ...login, email: e.target.value })}
        />
        <Input
          icon={Password}
          text="Password"
          type="password"
          isCorrect={validations(login).password}
          handleChange={e => setLogin({ ...login, password: e.target.value })}
        />
      </>
      <Button href={goToDashboard()}> Continue </Button>
      <BottomText> {`Terms & Conditions`}</BottomText>
    </Wrapper>
  );
};
