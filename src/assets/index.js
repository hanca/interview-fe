import Bookmark from "./icons/Bookmark.png";
import Hamburger from "./icons/Hamburger.png";
import Search from "./icons/Search.png";
import Add from "./icons/Add.png";
import Email from "./icons/Email.png";
import Job from "./icons/Job.png";
import Password from "./icons/Password.png";
import Username from "./icons/Username.png";
import Sync from "./icons/Sync.png";
import Upload from "./icons/Upload.png";
import Logout from "./icons/Logout.png";
import Collapse from "./icons/Collapse.png";
import Time from "./icons/Time.png";
import Source from "./icons/Source.png";

import { Logo } from "./Logo";

import UserPlaceholder from "./images/UserPlaceholder.png";
import AvatarBig from "./images/AvatarBig.png";

export {
  Bookmark,
  Hamburger,
  Search,
  Add,
  Logo,
  UserPlaceholder,
  Upload,
  Password,
  Job,
  Email,
  Sync,
  Username,
  AvatarBig,
  Logout,
  Collapse,
  Source,
  Time
};
