import React from "react";

export const Logo = () => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="23"
    height="22"
    viewBox="0 0 23 22"
  >
    <defs>
      <linearGradient
        id="5x6ya"
        x1="20.21"
        x2="19.27"
        y1="21.94"
        y2=".52"
        gradientUnits="userSpaceOnUse"
      >
        <stop offset="0" stopOpacity=".3" />
        <stop offset=".6" stopOpacity="0" />
      </linearGradient>
      <linearGradient
        id="5x6yb"
        x1="3.73"
        x2="2.78"
        y1="21.94"
        y2=".52"
        gradientUnits="userSpaceOnUse"
      >
        <stop offset=".4" stopOpacity="0" />
        <stop offset="1" stopOpacity=".3" />
      </linearGradient>
    </defs>
    <g>
      <g>
        <g>
          <path
            fill="#fff"
            d="M19.738.521a2.472 2.472 0 0 1 2.472 2.472v16.48a2.472 2.472 0 1 1-4.945 0V2.992A2.472 2.472 0 0 1 19.738.521z"
          />
          <path
            fill="url(#5x6ya)"
            d="M19.738.521a2.472 2.472 0 0 1 2.472 2.472v16.48a2.472 2.472 0 1 1-4.945 0V2.992A2.472 2.472 0 0 1 19.738.521z"
          />
        </g>
        <g>
          <path
            fill="#fff"
            d="M3.257.521a2.472 2.472 0 0 1 2.472 2.472v16.48a2.472 2.472 0 1 1-4.945 0V2.992A2.472 2.472 0 0 1 3.257.521z"
          />
          <path
            fill="url(#5x6yb)"
            d="M3.257.521a2.472 2.472 0 0 1 2.472 2.472v16.48a2.472 2.472 0 1 1-4.945 0V2.992A2.472 2.472 0 0 1 3.257.521z"
          />
        </g>
        <g>
          <path
            fill="#fff"
            d="M1.474 1.211a2.473 2.473 0 0 1 3.497 0l16.548 16.547a2.472 2.472 0 0 1-3.496 3.496L1.474 4.708a2.472 2.472 0 0 1 0-3.497z"
          />
        </g>
      </g>
    </g>
  </svg>
);
